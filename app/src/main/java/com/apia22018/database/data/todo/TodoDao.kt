package com.apia22018.database.data.todo

import android.arch.persistence.room.*

@Dao
interface TodoDao {

    @Insert
    fun create(vararg todo: Todo)

    @Query("SELECT * FROM todos")
    fun getAllTodos(): MutableList<Todo>

    @Update
    fun update(vararg todo: Todo)

    @Delete
    fun delete(vararg todo: Todo)
}