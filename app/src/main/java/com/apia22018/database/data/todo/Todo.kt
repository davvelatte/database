package com.apia22018.database.data.todo

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "todos")
data class Todo(
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "finish_at") var finishAt: String,
        @ColumnInfo(name = "message") var message: String
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}


fun testValues(): Array<Todo>{
    return arrayOf(
            Todo(name = "Clean the house", finishAt = "25/9", message = "You need to dust everything, don't forget to take the trash out!"),
            Todo(name = "By food", finishAt = "24/9", message = "Meat, pasta, milk, and eggs"),
            Todo(name = "Homework", finishAt = "28/9", message = "Need to complete database lab"),
            Todo(name = "Workout", finishAt = "20/9", message = "Run 10k"),
            Todo(name = "Pick up the baby", finishAt = "20/9", message = "She will be mad"),
            Todo(name = "Eat food", finishAt = "20/9", message = "Go to dinner with the girlfriend"),
            Todo(name = "Movie Time", finishAt = "30/9", message = "Have to book the tickets and then pick them up"),
            Todo(name = "Visit the parents", finishAt = "28/9", message = "Have to by flowers and som fika")
    )
}