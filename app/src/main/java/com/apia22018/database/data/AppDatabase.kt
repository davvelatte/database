package com.apia22018.database.data

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import android.content.Context
import com.apia22018.database.data.todo.Todo
import com.apia22018.database.data.todo.TodoDao
import com.apia22018.database.data.todo.testValues
import com.apia22018.database.screen.todo.TodoDataCallback
import com.apia22018.database.util.IO

@Database(entities = [Todo::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val todoDao: TodoDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AppDatabase::class.java, "database.db")
                                .addMigrations(MIGRATION_1_2)
                                .addCallback(databaseCreationCallback)
                                .build()

                    }
                }
            }
            return INSTANCE
        }

        private val databaseCreationCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                IO.execute {
                    INSTANCE?.todoDao?.create(* testValues())
                }
            }
        }

        fun destroy() {
            INSTANCE = null
        }

        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // DONT DO ANYTHING IN THIS MIGRATION JUST CHANGE THE VERSION NUMBER
            }
        }


    }
}