package com.apia22018.database.screen.todo

import com.apia22018.database.data.todo.Todo

interface TodoDataCallback {
    fun onSuccess(todos: MutableList<Todo>) {}
    fun onSuccess() {}
}