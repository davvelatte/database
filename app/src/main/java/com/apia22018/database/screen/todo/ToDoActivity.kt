package com.apia22018.database.screen.todo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.apia22018.database.R
import com.apia22018.database.data.AppDatabase
import com.apia22018.database.data.todo.Todo
import com.apia22018.database.data.todo.TodoDao
import com.apia22018.database.util.IO
import com.apia22018.database.util.UI
import kotlinx.android.synthetic.main.todo_activity.*

class ToDoActivity : AppCompatActivity() {
    var todoDao: TodoDao? = null
    val adapter: TodoAdapter = TodoAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.todo_activity)

        todoDao = AppDatabase.getInstance(this)?.todoDao

        todoList.layoutManager = LinearLayoutManager(this)

        todoList.adapter = adapter

        updateUI()
    }

    private fun loadTodos(callBack: TodoDataCallback) {
        IO.execute {
            val todos = todoDao?.getAllTodos() ?: mutableListOf()
            UI.execute {
                callBack.onSuccess(todos)
            }
        }
    }

    fun createTodo(view: View) {
        IO.execute {
            todoDao?.create(Todo("Change the tiers", "30/9", "Need to change to winter tires"))
            UI.execute {
                updateUI()
            }
        }
    }

    private fun updateUI() {
        loadTodos(object : TodoDataCallback {
            override fun onSuccess(todos: MutableList<Todo>) = adapter.setTodo(todos)
        })
    }
}
