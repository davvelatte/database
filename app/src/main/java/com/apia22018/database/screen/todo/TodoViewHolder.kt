package com.apia22018.database.screen.todo

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.apia22018.database.R
import com.apia22018.database.data.todo.Todo

class TodoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val mName = itemView.findViewById<TextView>(R.id.name)
    private val mFinishedAt = itemView.findViewById<TextView>(R.id.finishedAt)
    private val mMessage = itemView.findViewById<TextView>(R.id.message)

    companion object {
        fun newInstance(parent: ViewGroup) : TodoViewHolder {
            return TodoViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(
                                    R.layout.todo_list_item,
                                    parent,
                                    false
                            )
            )
        }
    }

    fun bind(todo: Todo) {
        with(todo) {
            mName.text = name
            mFinishedAt.text = finishAt
            mMessage.text = message
        }
    }
}