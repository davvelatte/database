package com.apia22018.database.screen.todo

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.apia22018.database.data.todo.Todo

class TodoAdapter : RecyclerView.Adapter<TodoViewHolder>() {
    private var mTodoList = mutableListOf<Todo>()

    fun setTodo(todoList: MutableList<Todo>) {
        this.mTodoList = todoList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int) = TodoViewHolder.newInstance(parent)

    override fun getItemCount() = mTodoList.size

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) = holder.bind(mTodoList[position])

}